package fr.ulille.iut.m4102;

class PasDAirException extends Exception {
    public PasDAirException(String message) {
        super(message);
    }
}
