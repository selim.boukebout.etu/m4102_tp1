package fr.ulille.iut.m4102;

import java.net.Socket;

/**
 * Cette classe se trouvera côté service
 * Elle recevra une requête sous forme de chaîne
 * de caractère conforme à votre protocole.
 * Elle transformera cette requête en une
 * invocation de méthode sur le service puis
 * renverra le résultat sous forme de chaîne
 * de caractères.
 */
public class AccesService {
	private AlaChaine alc;
	private Socket client;

	public AccesService() {
		alc = new AlaChaine();
	}
	
	public AccesService(Socket s) {
		alc = new AlaChaine();
		client = s;
	}
	
	public void traiteRequete() {}

	public String traiteInvocation(String invocation) {
		// ici, il faudra décomposer la chaîne pour retrouver la méthode appelée
		// et les paramètres, réaliser l'invocation sur la classe AlaChaine puis
		// renvoyer le résultat sous forme de chaîne.
		String str = invocation.substring(5);
		String cmd = str.substring(0,str.indexOf(":"));
		String param = "";
		switch(cmd) {

		case "nombreMots":
			param = str.substring(str.indexOf("\"")+1, str.lastIndexOf("\""));
			return "RET:param[int,"+ alc.nombreMots( param ) +"]";

		case "leetSpeak":
			param = str.substring(str.indexOf("\"")+1, str.lastIndexOf("\""));
			return "RET:param[string,"+ alc.leetSpeak(param)+"]";
		case "compteChar":
			param = str.substring(str.indexOf(','), str.indexOf(']'));
			char c = str.substring(str.lastIndexOf(','), str.lastIndexOf(']')).toCharArray()[0];
			return "RET:param[int,"+alc.compteChar( param, c )+"]";
		case "asphyxie":
			try {
				param = str.substring(str.indexOf("\"")+1, str.lastIndexOf("\""));
				return "RET:param[string,"+ alc.asphyxie(param)+"]";
			} catch (PasDAirException e) {
				return "RET:exception[PasDAirException:"+e.getMessage()+"]";
			}
		default:
			return "RET:";
		}
	}
}
