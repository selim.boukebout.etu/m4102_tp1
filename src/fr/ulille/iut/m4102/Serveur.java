package fr.ulille.iut.m4102;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import java.net.ServerSocket;
import java.net.Socket;

/**
 * Cette classe démarre une socket serveur et
 * attend les connexions des clients.
 * Quand un client se connecte, elle délègue
 * le travail à la classe AccesService.
 */
public class Serveur {
    private ServerSocket serveurSocket = null;
    AccesService ac = new AccesService();
    
    public Serveur(int port) {
	try {
	    // Création de la Socket Serveur qui permettra d'attendre les connexions
	    serveurSocket = new ServerSocket(port);
	} catch (IOException e) {
	    e.printStackTrace();
	    System.exit(1);
	}
    }
    
    public void miseEnService() {
	Socket unClient = null;
	
	// Boucle d'attente des clients
	while (true ) {
	    try {
		// accept() est bloquant. Quand on en sort, on a un nouveau
		// client avec une nouvelle instance de socket
		unClient = serveurSocket.accept();
	    } catch (IOException e) {
		e.printStackTrace();
		System.exit(1);
	    }

	    // quand on a un client, on peut instancier la
	    // classe AccesService et lui demander de traiter
	    // la requête.

	    AccesService as = new AccesService(unClient);
	    as.traiteRequete();
	}
    }
    
    // La classe doit être exécutée en passant le port serveur à utiliser en paramètre
    public static void main(String[] args) {
	Serveur serveur = new Serveur (Integer.parseInt(args[0]));
	
	serveur.miseEnService();
    }   
}
