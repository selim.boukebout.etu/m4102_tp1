package fr.ulille.iut.m4102;

/**
 * Cette classe introduit un intermédiaire entre la classe utilisatrice
 * et l'implémentation du traitement des chaînes.
 * Au début cette classe se contente de logger ce qui se passe puis
 * elle va évoluer pour accéder au service à distance.
 * Le comportement de cette classe est totalement transparent pour la
 * classe Utilisatrice qui au final utilise les mêmes méthodes que si elle
 * appelait directement la classe AlaChaine.
 */

public class Intermediaire implements AlaChaineInterface {
    private AlaChaine alc;
    AccesService ac = new AccesService();
    
    public Intermediaire() {
    	alc = new AlaChaine();
    }
    
    public int nombreMots(String chaine) {
    	String res = ac.traiteInvocation("CALL:nombreMots:param[string,\""+chaine+"\"]");
    	
    	if(res.contains("param")) {
    		
    		res = res.substring( res.indexOf(',')+1, res.lastIndexOf(']'));
    		if(res!=null && res!="") return Integer.valueOf(res);
    		
    	} else if (res.contains("exception")) {
    		
    		return -1;
    		
    	}
    	
		return -1;
    }

    public String asphyxie(String chaine) throws PasDAirException {
    	String res = ac.traiteInvocation("CALL:asphyxie:param[string,\""+chaine+"\"]");
    	
    	if(res.contains("param")) {
    		
    		res = res.substring(res.indexOf(',')+1, res.lastIndexOf(']') );
    		return res;
    		
    	} else if (res.contains("exception")) {
    		
    		res.replaceAll("RET:", "");
    		PasDAirException e = new PasDAirException(": Déjà Asphyxié");
    		throw e;
    		
    	}    	
		return "";
    }

    public String leetSpeak(String chaine) {
    	String res = ac.traiteInvocation("CALL:leetSpeak:param[string,\""+chaine+"\"]");
    	
    	if(res.contains("param")) {
    		
    		res = res.substring(res.indexOf(',')+1, res.lastIndexOf(']') );
    		return res;
    		
    	} else if (res.contains("exception")) {
    		
    		return "";
    		
    	}
    	
		return "";
    }

    public int compteChar(String chaine, char c) {
    	String res = ac.traiteInvocation("CALL:compteChar:param[string,\""+chaine+"\"]:param[char,\""+c+"\"]");
    	
    	if(res.contains("param")) {
    		
    		res = res.substring(res.indexOf(',')+1, res.lastIndexOf(']') );
    		if(res!=null && res!="") return Integer.valueOf(res);
    		
    	} else if (res.contains("exception")) {
    		
    		return -1;
    		
    	}
    	
		return -1;
    }
  

}